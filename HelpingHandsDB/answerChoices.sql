﻿CREATE TABLE [dbo].[answerChoices]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [activityId] INT NOT NULL, 
    [number] INT NOT NULL, 
    [text] VARCHAR(50) NULL
)
