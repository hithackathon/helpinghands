﻿CREATE TABLE [dbo].[groupActivity]
(
	[Id] INT IDENTITY (1,1) NOT NULL PRIMARY KEY, 
    [activityID] INT NOT NULL, 
    [groupID] INT NOT NULL, 
    [answerID] INT NOT NULL, 
    [timestamp] DATETIME NOT NULL, 
    CONSTRAINT [FK_groupActivity_activities] FOREIGN KEY ([activityID]) REFERENCES [activities]([Id]),
	CONSTRAINT [FK_groupActivity_groups] FOREIGN KEY ([groupID]) REFERENCES [groups]([Id]),
	CONSTRAINT [FK_groupActivity_answers] FOREIGN KEY ([answerID]) REFERENCES [answerChoices]([Id])
)
