﻿CREATE TABLE [dbo].[activities]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [name] VARCHAR(MAX) NOT NULL, 
    [text] VARCHAR(MAX) NOT NULL
)
