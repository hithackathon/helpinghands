﻿CREATE TABLE [dbo].[users]
(
	[Id] INT IDENTITY (1,1) NOT NULL PRIMARY KEY, 
    [email] VARCHAR(100) NOT NULL, 
    [password] VARCHAR(100) NOT NULL, 
    [groupID] INT NOT NULL, 
    CONSTRAINT [FK_users_groups] FOREIGN KEY ([groupID]) REFERENCES [groups]([Id])
)
