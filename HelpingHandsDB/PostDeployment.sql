﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/


MERGE INTO groups AS Target
USING (VALUES
(1, 'TeamA')
)
AS SOURCE (Id, name)
ON Target.Id = Source.Id
WHEN NOT MATCHED BY TARGET THEN
INSERT (Id, name)
VALUES (Id, name);


MERGE INTO users AS Target
USING (VALUES
(1, 'UserOne@Example.com', 'Password1', 1)
)
AS SOURCE (Id, email, password, groupID)
ON Target.Id = Source.Id
WHEN NOT MATCHED BY TARGET THEN
INSERT (email, password, groupID)
VALUES (email, password, groupID);


MERGE INTO activities AS Target
USING (VALUES
(1, 'Steps', 'How many steps did you complete today?'),
(2, 'Time/Duration', 'How long did you exorcise today?')
)
AS SOURCE (Id, name, text)
ON Target.Id = Source.Id
WHEN NOT MATCHED BY TARGET THEN
INSERT (Id, name, text)
VALUES (Id, name, text);

MERGE INTO answerChoices AS Target
USING (VALUES
(1, 1, 1, '0-2,500'),
(2, 1, 2, '2,500 - 5,000'),
(3, 1, 3, '5,000 - 7,500'),
(4, 1, 4, '7,500 - 10,000'),
(5, 1, 5, '10,000+'),
(6, 2, 1, '0 - 15m'),
(7, 2, 2, '15 - 30m'),
(8, 2, 3, '30m - 45m'),
(9, 2, 4, '45m - 60m'),
(10, 2, 5, '60+m')
)
AS SOURCE (Id, activityID, number, text)
ON Target.Id = Source.Id
WHEN NOT MATCHED BY TARGET THEN
INSERT (Id, activityID, number, text)
VALUES (Id, activityID, number, text);