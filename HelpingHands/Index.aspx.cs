﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpingHands
{
    public partial class Index : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
                if (Session["groupID"] != null)
                {
                    Response.Redirect("Activity.aspx");
                }
                else
                {
                    Response.Redirect("Login.aspx");
                }
            
        }
    }
}