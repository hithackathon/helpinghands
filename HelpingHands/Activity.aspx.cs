﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace HelpingHands
{
    public partial class Activity1 : System.Web.UI.Page
    {
        protected bool isSelected { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["groupID"] == null)
            {
                Response.Redirect("Login.aspx");
            }


        }

        

        protected void steps_Click(object sender, EventArgs e)
        {
            lblSelectedActivity.Text = "1";
            string str = ConfigurationManager.ConnectionStrings["MyConn"].ToString();
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT[text] FROM[activities] WHERE [Id] = '1'";
            cmd.CommandType = System.Data.CommandType.Text;

            SqlDataReader myDataReader;
            myDataReader = cmd.ExecuteReader();
            if (myDataReader.Read())
            {
                lblActivityText.Text = myDataReader["text"].ToString();
            }
            myDataReader.Close();


            isSelected = true;
            sdsQuestions.SelectCommand = "SELECT[Id], [text] FROM[answerChoices] WHERE activityID='1' ORDER BY [number] ASC";
            rbQuestions.Items.Clear();
            rbQuestions.DataBind();
            rbQuestions.Visible = true;
            btnSubmit.Visible = true;
            lblSubmitMessage.Text = "";
        }

        protected void TimeDuration_Click(object sender, EventArgs e)
        {
            lblSelectedActivity.Text = "2";
            string str = ConfigurationManager.ConnectionStrings["MyConn"].ToString();
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            cmd.CommandText = "SELECT[text] FROM[activities] WHERE [Id] = '2'";
            cmd.CommandType = System.Data.CommandType.Text;

            SqlDataReader myDataReader;
            myDataReader = cmd.ExecuteReader();
            if (myDataReader.Read())
            {
                lblActivityText.Text = myDataReader["text"].ToString();
            }
            myDataReader.Close();


            isSelected = true;
            sdsQuestions.SelectCommand = "SELECT[Id], [text] FROM[answerChoices] WHERE activityID='2' ORDER BY [number] ASC";
            rbQuestions.Items.Clear();
            rbQuestions.DataBind();
            rbQuestions.Visible = true;
            btnSubmit.Visible = true;
            lblSubmitMessage.Text = "";

        }

        protected void btnSubmit_Click(object sender, EventArgs e)
        {
            string str = ConfigurationManager.ConnectionStrings["MyConn"].ToString();
            SqlConnection conn = new SqlConnection(str);
            conn.Open();
            SqlCommand cmd = conn.CreateCommand();
            int activityID = int.Parse(lblSelectedActivity.Text);
            int groupID = int.Parse(Session["groupID"].ToString());
            int answerID = int.Parse(rbQuestions.SelectedValue);
            DateTime timestamp = DateTime.Now;
            cmd.CommandText = "INSERT INTO groupActivity (activityID, groupID, answerID, timestamp) VALUES ('" + activityID + "', '" + groupID + "', '" + answerID + "', '" + timestamp + "')";
            cmd.CommandType = System.Data.CommandType.Text;
            cmd.ExecuteNonQuery();
            lblSubmitMessage.Text = "Thank you for your submisson";
            lblSubmitMessage.BackColor = Color.FromArgb(6, 61, 48);
            lblSubmitMessage.ForeColor = Color.White;
            lblSubmitMessage.Font.Bold = true;
            rbQuestions.Visible = false;
            btnSubmit.Visible = false;
            
        }
    }
}