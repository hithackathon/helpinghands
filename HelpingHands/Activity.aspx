﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="Activity.aspx.cs" Inherits="HelpingHands.Activity1" Async="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="center" runat="server">



    <div class="col-sm-8 col-sm-offset-2">
        <h3 style="margin-top: 0em; padding-top: 0em;">Thank you for participating in our Health and Wellness Program.
        </h3>
        <h3>The information entered into this system is tracked on a group basis and does personally identify users.
        </h3>
        <br />
        <br />
    </div>

    <div class="col-sm-6 col-lg-offset-3">
        <asp:Label ID="lblSubmitMessage" runat="server" Text="" CssClass="customSubmitMessage"></asp:Label>
        <label>Please Select What Type Of Activity You Would Like to Enter</label>
        <br />
        <asp:Button runat="server" ID="steps" Text="Steps" OnClick="steps_Click" CssClass="btn btn-primary" />
        <asp:Button runat="server" ID="TimeDuration" Text="Time/Duration" OnClick="TimeDuration_Click" CssClass="btn btn-primary" />
        <br />

        <% if (isSelected == true)
            {%>
        <asp:SqlDataSource ID="sdsQuestions" runat="server" ConnectionString="<%$ ConnectionStrings:MyConn %>"></asp:SqlDataSource>
        <asp:Label runat="server" Text="" ID="lblActivityText"></asp:Label>
        <asp:RadioButtonList ID="rbQuestions" runat="server" DataSourceID="sdsQuestions" DataValueField="Id" DataTextField="text" Visible="false" AppendDataBoundItems="true"></asp:RadioButtonList>
        <br />
        <h5 runat="server" id="submitStatement">I am submitting this information voluntarily and certify it is true and accurate. I also understand any false information submitted is a violation of our company's code of ethics and subject to disciplinary action up to and including termination :)
        </h5>
        <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Visible="false" CssClass="btn btn-success" BackColor="#063d30" />


        <br />
        <br />
        <br />

        <asp:Label ID="lblSelectedActivity" runat="server" Text="Label" Visible="false"></asp:Label>
        <%}%>
    </div>




</asp:Content>
